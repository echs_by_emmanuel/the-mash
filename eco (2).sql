-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2019 at 07:57 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eco`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `quantity`) VALUES
(1, 21, 37, 1),
(6, 15, 33, 1),
(7, 15, 31, 3);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `cat_slug` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `cat_slug`) VALUES
(5, 'Category A', '1'),
(6, 'Category B', '2'),
(7, 'Category C', '3'),
(8, 'Category D', '4');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `sales_id`, `product_id`, `quantity`) VALUES
(21, 11, 34, 1),
(22, 11, 37, 3),
(23, 14, 33, 3),
(24, 15, 33, 1),
(25, 15, 36, 4),
(26, 16, 31, 1),
(27, 17, 33, 1),
(28, 18, 33, 1),
(29, 19, 33, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `Odate` date NOT NULL,
  `Dtime` text NOT NULL,
  `ship_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(200) NOT NULL,
  `price` double NOT NULL,
  `photo` varchar(200) NOT NULL,
  `date_view` date NOT NULL,
  `counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `slug`, `price`, `photo`, `date_view`, `counter`) VALUES
(31, 5, 'Food A', '<p>Description Here</p>\r\n', 'food', 120, 'food.jpeg', '2019-07-27', 2),
(32, 5, 'Food B', '<p>Description .....</p>\r\n', 'food-b', 166, 'food-b.jpeg', '0000-00-00', 0),
(33, 6, 'Food C', '<p>Description .....</p>\r\n', 'food-c', 123, 'food-c.jpeg', '2019-06-22', 2),
(34, 6, 'Food D', '<p>Description ...</p>\r\n', 'food-d', 266, 'food-d.jpeg', '2019-06-10', 1),
(35, 7, 'Food E', '<p>Description Here ...</p>\r\n', 'food-e', 321, 'food-e.jpeg', '0000-00-00', 0),
(36, 7, 'Food F', '<p>Description Here ..</p>\r\n', 'food-f', 70, 'food-f.jpeg', '2019-06-22', 1),
(37, 8, 'Food G', '<p>Description Here ....</p>\r\n', 'food-g', 33, 'food-g.jpeg', '2019-06-12', 8),
(38, 8, 'Food H', '<p>Description Here</p>\r\n', 'food-h', 454, 'food-h.jpeg', '2019-06-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pay_id` varchar(50) NOT NULL,
  `sales_date` date NOT NULL,
  `method` text NOT NULL,
  `Otime` text NOT NULL,
  `Odate` date NOT NULL,
  `H_recieve_type` text NOT NULL,
  `contact_person` text NOT NULL,
  `cell` int(12) NOT NULL,
  `D_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `user_id`, `pay_id`, `sales_date`, `method`, `Otime`, `Odate`, `H_recieve_type`, `contact_person`, `cell`, `D_address`) VALUES
(11, 15, '1', '2019-06-12', 'card', '0', '0000-00-00', '0', '0', 0, '0'),
(12, 15, '1', '2019-06-12', 'card', '0', '0000-00-00', '0', '0', 0, '0'),
(13, 15, '1', '2019-06-12', 'card', '0', '0000-00-00', '0', '0', 0, '0'),
(14, 15, '1', '2019-06-22', 'cash', '0', '0000-00-00', '0', '0', 0, '0'),
(15, 15, '1', '2019-06-11', 'cash', '0', '0000-00-00', '0', '0', 0, '0'),
(16, 15, '1', '2019-06-22', 'cash', '23:59', '2019-06-11', '', '', 0, ''),
(17, 15, '1', '2019-06-22', 'cash', '23:59', '2019-12-31', 'cash', '', 0, ''),
(18, 15, '1', '2019-06-22', 'cash', '23:59', '2019-12-31', 'cash', 'Emmm', 0, ''),
(19, 15, '1', '2019-06-22', 'cash', '23:59', '2019-12-31', 'cash', 'Emmm', 4444, 'dfcdfcghjkl');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(60) NOT NULL,
  `type` int(1) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `contact_info` varchar(100) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `status` int(1) NOT NULL,
  `activate_code` varchar(15) NOT NULL,
  `reset_code` varchar(15) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `type`, `firstname`, `lastname`, `address`, `contact_info`, `photo`, `status`, `activate_code`, `reset_code`, `created_on`) VALUES
(14, 'admin@admin.com', '$2y$10$lbVnJx3CYMNNXj4n5oL27uSQ1agPoKr37wRrsij1UHH0omxd0sf6.', 1, 'System', 'Admin', '', '', '', 1, 'sRHZ1kbULhdt', '', '2019-06-10'),
(15, 'user@user.com', '$2y$10$0MoOUvtzAQ.USFpqNqCqceZWGM2.O7ZsaL2mE7pDsz9dLWj4SvHl2', 0, 'user', 'User', '', '', '', 1, 'gytCfpBxJuAN', '', '2019-06-10'),
(16, 'e@e.com', '$2y$10$9s5K6RAo2Ba6sL3cttqUpOPrds.0rxOL2vVJF.AX33wrxrg2EVZDS', 0, 'Emmanuel', 'Shekinah', '', '', '', 0, 'fDpMXOHR3TuG', '', '2019-06-12'),
(17, 'e@e.comq', '$2y$10$a9W5sw3FRi9HO/KSyxlGTu.jOS.u.bAkiyOy/KIj/yM2WVa8p6eNu', 0, 'Emmanuel', 'Shekinah', '', '', '', 0, 'zBeRXjnObqW7', '', '2019-06-12'),
(18, 'user@user.comg', '$2y$10$Am26.Ft5qnjcc5ZffTVHD.Qe/2gCrb4fi9iLAXyS42hdrqEDUVdma', 0, 'Emmanuel', 'Shekinah', '', '', '', 0, 'RuifchoEV8Ts', '', '2019-06-12'),
(19, 'admin@gmail.comaa', '$2y$10$dBu7ak.it/OKooxxJHEVneT2XdTLVpNjt.yBMZtVN113hSSxKEP6m', 0, 'Emmanuel', 'Shekinah', '', '', '', 0, 'aDiY3qZ2pyfm', '', '2019-06-12'),
(20, 't@t.t', '$2y$10$qCSAY9tkuJESap6NDw9gjerHjUpT3KvVtDX2ns8zBPXdtzRvjSUlm', 0, 't', 't', '', '', '', 0, 'ur29vyGDcfqg', '', '2019-06-12'),
(21, 'y@y.y', '$2y$10$IpbMV.ZOFkd/vvKRlnZtH.P3DYW3iGZTfXuacOPfNtWDoG9.1Mo4K', 0, 'y', 'y', '', '', '', 0, 'ZwIo62MdxhtT', '', '2019-06-12'),
(22, 'u@u.u', '$2y$10$glLdShcfPONMVf1uPWnkFuf/fTgdCO0K0VwKwxLcxW.pnqyIhL4ea', 0, 'tfghdhjghj', 'y', '', '', '', 0, 'tE7cSz3vkyeU', '', '2019-06-20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
