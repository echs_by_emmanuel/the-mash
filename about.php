<?php include 'includes/session.php'; ?>
<?php include 'includes/header.php'; ?>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

    <?php include 'includes/navbar.php'; ?>

    <div class="content-wrapper">
        <div class="container">

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-sm-9">
                        <?php
                        if(isset($_SESSION['error'])){
                            echo "
	        					<div class='alert alert-danger'>
	        						".$_SESSION['error']."
	        					</div>
	        				";
                            unset($_SESSION['error']);
                        }
                        ?>
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <br>
                            <br>
                            <br>
                            <br>
                            Innovative spin on your favourite foods, including deep dish pizzas, salads, sandwiches, pastas, steaks,
                            baby back ribs and so much more. The restaurant has become a
                            family favourite all across the SA and caters for diners of all ages. Their handcrafted beer puts the proverbial cherry on the cake.
                        </div>

                    </div>
                    <div class="col-sm-3">
                        <?php include 'includes/sidebar.php'; ?>
                    </div>
                </div>
            </section>

        </div>
    </div>

    <?php include 'includes/footer.php'; ?>
</div>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
