<?php
	include 'includes/session.php';
	$conn = $pdo->open();

	if(isset($_POST['login'])){

		$email = $_POST['email'];
		$password = $_POST['password'];

		try{

			$stmt = $conn->prepare("SELECT *, COUNT(*) AS numrows FROM users WHERE email = :email");
			$stmt->execute(['email'=>$email]);
			$row = $stmt->fetch();
			if($row['numrows'] > 0){
				if($row['status']){
					if(password_verify($password, $row['password'])){
						if($row['type']){
							admin:
							$_SESSION['admin'] = $row['id'];

							if(!isset($_SESSION['admin'])){
								goto admin;
							}
						}
						else{
							user:
							$_SESSION['user'] = $row['id'];
                            if(!isset($_SESSION['user'])){
                                goto user;
                            }
						}
					}
					else{
						$_SESSION['error'] = 'Incorrect Password';
					}
				}
				else{
					//$_SESSION['error'] = 'Account not activated.';
                    user2:
                    $_SESSION['user'] = $row['id'];
                    if(!isset($_SESSION['user'])){
                        goto user2;
                    }


				}
			}
			else{
				$_SESSION['error'] = 'Email not found';
			}
		}
		catch(PDOException $e){
			echo "There is some problem in connection: " . $e->getMessage();
		}

	}
	else{
		$_SESSION['error'] = 'Input login credentails first';
	}

	$pdo->close();

	header('location: login.php');

?>
